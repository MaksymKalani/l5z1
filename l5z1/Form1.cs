﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace l5z1
{
    public partial class Form1 : Form
    {
        private l5z1Context _db;
        private List<Table> tables;
        public Form1()
        {
            _db = new l5z1Context();
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tables = _db.Table.ToList();
            dataGridView1.DataSource = tables;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
